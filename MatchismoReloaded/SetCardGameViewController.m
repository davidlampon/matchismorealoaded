//
//  SetCardGameViewController.m
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 13/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import "SetCardGameViewController.h"
#import "SetCardDeck.h"
#import "SetCard.h"
#import "SetCardGameCollectionViewCell.h"
#import "SetCardView.h"

@interface SetCardGameViewController ()

@property (nonatomic) int numberOfCards;

@end

@implementation SetCardGameViewController

- (int)numberOfCards
{
    if (!_numberOfCards) _numberOfCards = 3;
    return _numberOfCards;
}

- (Deck *)createDeck
{
    self.gameType = @"Set Cards";
    return [[SetCardDeck alloc] init];
}

- (NSUInteger)startingCardCount
{
    return 12;
}

- (void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card animate:(BOOL)animate redeal:(BOOL)redeal
{
    if ([cell isKindOfClass:[SetCardGameCollectionViewCell class]])
    {
        SetCardView *setCardView = ((SetCardGameCollectionViewCell *)cell).setCardView;
        if ([card isKindOfClass:[SetCard class]])
        {
            SetCard *setCard = (SetCard *)card;
            
            if ((animate == YES) && ((setCard.chosen && !setCardView.faceUp) || (!setCard.chosen && setCardView.faceUp)))
            {
                 [self animateFlipCardView:setCardView];
            }
            
            if (animate == NO && redeal == TRUE)
            {
                [self animateRotation:setCardView];
            }
            
            setCardView.number = setCard.number;
            setCardView.symbol = setCard.symbol;
            setCardView.shading = setCard.shading;
            setCardView.color = setCard.color;
            setCardView.faceUp = setCard.isChosen;
            setCardView.alpha = setCard.isMatched ? 0.3 : 1.0;
        }
    }
}

- (void)animateFlipCardView:(SetCardView *)cell
{
    [UIView transitionWithView:cell
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:nil
                    completion:nil];
}

- (void)animateRotation:(SetCardView *)cell
{
    [UIView animateWithDuration:0.25
                     animations:^{
                         [cell setTransform:CGAffineTransformMakeRotation(M_PI)];
                     }
                     completion:^(BOOL finished) {
                         if (finished) [UIView animateWithDuration:0.25
                                                        animations:^{
                                                            [cell setTransform:CGAffineTransformMakeRotation(M_PI*2)];
                                                        }
                                                        completion:nil];
                     }];
    
}

@end
