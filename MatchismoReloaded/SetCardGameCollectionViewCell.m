//
//  SetCardGameCollectionViewCell.m
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 26/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import "SetCardGameCollectionViewCell.h"

@implementation SetCardGameCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
