//
//  PlayingCardGameViewController.m
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 13/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import "PlayingCardGameViewController.h"
#import "PlayingCardDeck.h"
#import "PlayingCard.h"
#import "PlayCardGameViewController.h"
#import "PlayingCardView.h"
#import "PlayingCardGameCollectionViewCell.h"

@interface PlayingCardGameViewController ()
@property (nonatomic) int numberOfCards;
@end

@implementation PlayingCardGameViewController

- (int)numberOfCards
{
    if (!_numberOfCards) _numberOfCards = 2;
    return _numberOfCards;
}

- (Deck *)createDeck
{
    self.gameType = @"Playing Cards";
    return [[PlayingCardDeck alloc] init];
}

- (NSUInteger)startingCardCount
{
    return 20;
}

- (void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card animate:(BOOL)animate
{
    if ([cell isKindOfClass:[PlayingCardGameCollectionViewCell class]])
    {
        PlayingCardView *playingCardView = ((PlayingCardGameCollectionViewCell *)cell).playingCardView;
        if ([card isKindOfClass:[PlayingCard class]])
        {
            PlayingCard *playingCard = (PlayingCard *)card;
            playingCardView.rank = playingCard.rank;
            playingCardView.suit = playingCard.suit;
            playingCardView.faceUp = playingCard.isChosen;
            playingCardView.alpha = playingCard.isMatched ? 0.3 : 1.0;
        }
    }    
}

@end
