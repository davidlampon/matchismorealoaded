//
//  CardGameViewController.h
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 06/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardMatchingGame.h"
#import "Deck.h"
#import "GameResult.h"
#import "GameSettings.h"


@interface CardGameViewController : UIViewController

@property (strong, nonatomic) NSString *gameType;
@property (strong, nonatomic) GameResult *gameResult;
@property (nonatomic) NSUInteger startingCardCount;

//Abstract
- (Deck *)createDeck;
- (void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card animate:(BOOL)animate redeal:(BOOL)redeal;
@property (nonatomic) BOOL redeal;

@end
