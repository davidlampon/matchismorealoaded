//
//  PlayingCardGameViewController.h
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 13/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import "CardGameViewController.h"

@interface PlayingCardGameViewController : CardGameViewController

@end
