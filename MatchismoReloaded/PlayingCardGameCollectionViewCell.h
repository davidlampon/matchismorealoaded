//
//  PlayingCardGameCollectionViewCell.h
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 26/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayingCardView.h"

@interface PlayingCardGameCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet PlayingCardView *playingCardView;
@end
