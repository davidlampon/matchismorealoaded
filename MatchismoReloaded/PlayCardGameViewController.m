//
//  PlayCardGameViewController.m
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 12/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import "PlayCardGameViewController.h"
#import "PlayingCardDeck.h"

@interface PlayCardGameViewController ()

@end

@implementation PlayCardGameViewController

- (PlayingCardDeck *)createDeck
{
    PlayingCardDeck *deck = [[PlayingCardDeck alloc] init];
    return deck;
}

@end
