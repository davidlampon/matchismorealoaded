//
//  SetCard.h
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 12/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import "Card.h"

@interface SetCard : Card

@property (nonatomic) int number;
@property (strong, nonatomic) NSString *symbol;
@property (nonatomic) int shading;
@property (nonatomic) int color;

+ (NSArray *)validSymbols;

@end
