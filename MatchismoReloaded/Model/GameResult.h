//
//  GameResult.h
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 18/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameResult : NSObject

+ (NSArray *)allGameResults; // of GameResults

@property (readonly, nonatomic) NSDate *start;
@property (readonly, nonatomic) NSDate *end;
@property (readonly, nonatomic) NSTimeInterval duration;
@property (nonatomic) int score;
@property (strong, nonatomic) NSString *gameType;

- (NSComparisonResult)compareScore:(GameResult *)result;
- (NSComparisonResult)compareDuration:(GameResult *)result;
- (NSComparisonResult)compareDate:(GameResult *)result;

@end
