//
//  GameSettings.h
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 19/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameSettings : NSObject

@property (nonatomic) int matchBonus;
@property (nonatomic) int mismatchPenalty;
@property (nonatomic) int flipCost;

@end
