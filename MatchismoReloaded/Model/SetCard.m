//
//  SetCard.m
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 12/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import "SetCard.h"

@implementation SetCard

// Override match of Card

- (int)match:(NSArray *)otherCards
{
    int score = 0;
    
    NSMutableArray *cardNumbers = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:self.number], nil];
    NSMutableArray *cardSymbols = [[NSMutableArray alloc] initWithObjects:self.symbol, nil];
    NSMutableArray *cardShadings = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:self.shading], nil];
    NSMutableArray *cardColors = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:self.color], nil];
    
    if ([otherCards count])
    {
        for (id card in otherCards)
        {
            if ([card isKindOfClass:[SetCard class]])
            {
                SetCard *otherCard = (SetCard *)card;
                
                if (![cardNumbers containsObject:[NSNumber numberWithInt:otherCard.number]])
                {
                    [cardNumbers addObject:[NSNumber numberWithInt:otherCard.number]];
                }
                
                if (![cardSymbols containsObject:otherCard.symbol])
                {
                    [cardSymbols addObject:otherCard.symbol];
                }
                
                if (![cardShadings containsObject:[NSNumber numberWithInt:otherCard.shading]])
                {
                    [cardShadings addObject:[NSNumber numberWithInt:otherCard.shading]];
                }
                
                if (![cardColors containsObject:[NSNumber numberWithInt:otherCard.color]])
                {
                    [cardColors addObject:[NSNumber numberWithInt:otherCard.color]];
                }
            }
        }
        //NSLog(@"\nNumber: %d\n Symbol: %d\n Shading: %d\n Color: %d\n", [cardNumbers count], [cardSymbols count], [cardShadings count], [cardColors count]);
        
        /* Original game - Score if matching
        if( (([cardNumbers count] == 1) && ([cardSymbols count] == 3) && ([cardShadings count] == 3) && ([cardColors count] == 3)) ||
            (([cardNumbers count] == 3) && ([cardSymbols count] == 1) && ([cardShadings count] == 3) && ([cardColors count] == 3)) ||
            (([cardNumbers count] == 3) && ([cardSymbols count] == 3) && ([cardShadings count] == 1) && ([cardColors count] == 3)) ||
            (([cardNumbers count] == 3) && ([cardSymbols count] == 3) && ([cardShadings count] == 3) && ([cardColors count] == 1))    )
         */
        
        // New scoring permissive approach
        if( (([cardNumbers count] == 1) && ([cardSymbols count] >= 1) && ([cardShadings count] >= 1) && ([cardColors count] >= 1)) ||
            (([cardNumbers count] >= 1) && ([cardSymbols count] == 1) && ([cardShadings count] >= 1) && ([cardColors count] >= 1)) ||
            (([cardNumbers count] >= 1) && ([cardSymbols count] >= 1) && ([cardShadings count] == 1) && ([cardColors count] >= 1)) ||
            (([cardNumbers count] >= 1) && ([cardSymbols count] >= 1) && ([cardShadings count] >= 1) && ([cardColors count] == 1))    )
        {
            score = 1;
        }
    }
    
    return score;
}

+ (NSArray *)validSymbols
{
    return @[@"▲",@"●",@"◼︎"];
}

// Attributed string content

- (NSString *)contents
{
    return [NSString stringWithFormat:@"%d%@%d%d", self.number, self.symbol, self.shading, self.color];
}

@end
