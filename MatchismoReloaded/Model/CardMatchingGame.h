//
//  CardMatchingGame.h
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 06/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface CardMatchingGame : NSObject

// read only = no public setter - logic will handle the result
@property (nonatomic, readonly) NSInteger score;

// Number of cards
@property (nonatomic) int numberOfCards;

// Last play data
@property (nonatomic, readonly) NSInteger lastScore;
@property (nonatomic, readonly) NSMutableArray *lastMatchingCards;

//designated initializer
- (instancetype)initWithCardCount:(NSUInteger)count
                        usignDeck:(Deck *)deck;
- (void)chooseCardAtIndex:(NSUInteger)index;
- (Card *)cardAtIndex:(NSUInteger)index;

// Settings
@property (nonatomic) int matchBonus;
@property (nonatomic) int mismatchPenalty;
@property (nonatomic) int flipCost;

// Card count
@property (nonatomic) int cardCount;
- (void)removeCard:(Card *)card;
- (void)addThreeCardsToGameWithDeck:(Deck *)deck;

@end
