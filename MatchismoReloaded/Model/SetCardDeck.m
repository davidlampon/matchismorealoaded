//
//  SetCardDeck.m
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 12/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import "SetCardDeck.h"
#import "SetCard.h"

@implementation SetCardDeck

- (instancetype)init
{
    self = [super init];
    
    if(self)
    {
        for (int number = 1; number <= 3; number++) {
            for (NSString *symbol in [SetCard validSymbols]) {
                for (int shading = 1; shading <= 3; shading++) {
                    for (int color = 1; color <= 3; color++) {
                        
                        SetCard *card = [[SetCard alloc] init];
                        card.number = number;
                        card.symbol = symbol;
                        card.shading = shading;
                        card.color = color;
                        [self addCard:card];
                    }
                }                
            }
        }
        
    }
    
    return self;
}

@end
