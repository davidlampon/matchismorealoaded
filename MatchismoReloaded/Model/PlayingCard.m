//
//  PlayingCard.m
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 06/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import "PlayingCard.h"

@implementation PlayingCard

// Override match of Card
- (int)match:(NSArray *)otherCards
{
    int score = 0;
    NSMutableArray *cardSuits = [[NSMutableArray alloc] initWithObjects:self.suit, nil];
    NSMutableArray *cardRanks = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:self.rank], nil];
    
    if ([otherCards count])
    {
        for (id card in otherCards)
        {
            if ([card isKindOfClass:[PlayingCard class]])
            {
                PlayingCard *otherCard = (PlayingCard *)card;
                
                if (![cardSuits containsObject:otherCard.suit])
                {
                    [cardSuits addObject:otherCard.suit];
                }
                
                if (![cardRanks containsObject:[NSNumber numberWithInt:otherCard.rank]])
                {
                    [cardRanks addObject:[NSNumber numberWithInt:otherCard.rank]];
                }
            }
        }
        
        // Score: (total cards - different cards) * bonus_multiplier
        if([otherCards count]+1 != [cardSuits count])
        {
            score += ([otherCards count] - [cardSuits count] + 1) * 1;
        }
        
        if([otherCards count]+1 != [cardRanks count])
        {
            score += ([otherCards count] - [cardRanks count] + 1) * 4;
        }        
    }
    
    return score;
}

// Instance methods

+ (NSArray *)rankStrings
{
    return @[@"?",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"];
}

+ (NSArray *)validSuits
{
    return @[@"♠︎",@"♣︎",@"♥︎",@"♦︎"];
}

+ (NSUInteger)maxRank
{
    return [[PlayingCard rankStrings] count]-1;
}

// Setters and getters

- (NSString *)contents
{
    return [[PlayingCard rankStrings][self.rank] stringByAppendingString:self.suit];
}

- (void)setRank:(NSUInteger)rank
{
    if (rank <= [PlayingCard maxRank])
    {
       _rank = rank;
    }
}

@synthesize suit = _suit;

- (NSString *)suit
{
    return _suit ? _suit : @"?";
}

- (void)setSuit:(NSString *)suit
{
    if ([[PlayingCard validSuits] containsObject:suit])
    {
        _suit = suit;
    }
}

@end
