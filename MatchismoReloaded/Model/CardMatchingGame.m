//
//  CardMatchingGame.m
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 06/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame()

// Readwrite is the default
@property (nonatomic, readwrite) NSInteger score;
@property (nonatomic, strong) NSMutableArray *cards; //of Cards

// Last play data
@property (nonatomic, readwrite) NSInteger lastScore;
@property (nonatomic, readwrite) NSMutableArray *lastMatchingCards;
@end

@implementation CardMatchingGame

- (void)addThreeCardsToGameWithDeck:(Deck *)deck
{
    if (deck)
    {
        for (int i = 0; i < 3; i++)
        {
            Card *card = [deck drawRandomCard];
            if (card){
                [self.cards addObject:card];
            } else {
                break;
            }
        }
    }
}

- (NSMutableArray *)lastMatchingCards
{
    if (!_lastMatchingCards) _lastMatchingCards = [[NSMutableArray alloc] init];
    return _lastMatchingCards;
}

- (void)removeCard:(Card *)card
{
    [self.cards removeObject:card];    
}

@synthesize cards = _cards;

- (NSMutableArray *)cards
{
    if (!_cards) _cards = [[NSMutableArray alloc] init];
    return _cards;
}

- (void)setCards:(NSMutableArray *)cards
{
    _cards = cards;
}

-(int)cardCount
{
    return [self.cards count];
}

// #define MISMATCH_PENALTY 2
static const int MISMATCH_PENALTY = 2;
static const int MATCH_BONUS = 4;
static const int COST_TO_CHOOSE = 1;

- (instancetype)initWithCardCount:(NSUInteger)count
                        usignDeck:(Deck *)deck
{
    // default initializer
    self = [super init];
    
    if (self)
    {
        for (int i = 0; i < count; i++)
        {
            Card *card = [deck drawRandomCard];
            if (card){
                //self.cards[i] = card;
                [self.cards addObject:card];
            } else {
                self = nil;
                break;
            }
        }
        
        _matchBonus = MATCH_BONUS;
        _mismatchPenalty = MISMATCH_PENALTY;
        _flipCost = COST_TO_CHOOSE;
    }
    
    return (self);
}

- (instancetype)init
{
    return nil;
}

- (void)chooseCardAtIndex:(NSUInteger)index
{
    Card *card = [self.cards objectAtIndex:index];
    self.lastScore = 0;
    self.lastMatchingCards = nil;
    
    if (!card.isMatched)
    {
        if (card.isChosen) {
            card.chosen = NO;
        } else {
            // match against another card
            for (Card *otherCard in self.cards)
            {
                if (otherCard.isChosen && !otherCard.isMatched) {
                    [self.lastMatchingCards addObject:otherCard];
                }
            }
            
            if ([self.lastMatchingCards count] == self.numberOfCards-1)
            {                
                self.lastScore = [card match:self.lastMatchingCards];
                
                if (self.lastScore) {
                    self.lastScore += self.lastScore * self.matchBonus;
                    card.matched = YES;
                    
                    for (Card *matchedCard in self.lastMatchingCards){
                        matchedCard.matched = YES;
                    }
                    
                } else {
                    self.lastScore = -self.mismatchPenalty;
                    for (Card *chosenCard in self.lastMatchingCards){
                        chosenCard.chosen = NO;                    }
                }
            }
            
            self.lastScore = self.lastScore - self.flipCost;
            card.chosen = YES;
            
            self.score += self.lastScore;
        }
    }
}

- (Card *)cardAtIndex:(NSUInteger)index
{
    return (index < [self.cards count]) ? self.cards[index] : nil;
}

@end
