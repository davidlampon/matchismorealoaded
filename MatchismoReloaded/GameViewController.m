//
//  GameViewController.m
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 12/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import "GameViewController.h"
#import "PlayingCardDeck.h"
#import "CardMatchingGame.h"

@interface GameViewController ()
@property (nonatomic, strong) Deck *deck;
@property (nonatomic, strong) CardMatchingGame *game;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *numberOfCardsSegmentedControl;
@property (weak, nonatomic) IBOutlet UILabel *lastFlipLabel;
@property (weak, nonatomic) IBOutlet UISlider *historySlider;
@property (nonatomic) int numberOfCards;
@end

@implementation GameViewController

- (int)numberOfCards
{
    if (!_numberOfCards) _numberOfCards = 2;
    return _numberOfCards;
}

- (CardMatchingGame *)game
{
    if (!_game) _game = [[CardMatchingGame alloc] initWithCardCount:[self.cardButtons count]
                                                          usignDeck:[self createDeck]];
    _game.numberOfCards = self.numberOfCards;
    
    return _game;
}

-(Deck *)deck
{
    if (!_deck) _deck = [self createDeck];
    return _deck;
}

- (Deck *)createDeck
{
    return [[PlayingCardDeck alloc] init];
}

- (IBAction)touchCardButton:(UIButton *)sender
{
    self.numberOfCardsSegmentedControl.enabled = NO;
    int cardIndex = [self.cardButtons indexOfObject:sender];
    [self.game chooseCardAtIndex:cardIndex];
    self.historySlider.value = self.historySlider.maximumValue;
    
    self.lastFlipLabel.textColor = [UIColor blackColor];
    self.lastFlipLabel.text = self.game.lastFlip;
    
    [self updateUI];
}

- (void)updateUI
{
    for (UIButton *cardButton in self.cardButtons)
    {
        int cardIndex = [self.cardButtons indexOfObject:cardButton];
        Card *card = [self.game cardAtIndex:cardIndex];
        [cardButton setTitle:[self titleForCard:card]
                    forState:UIControlStateNormal];
        [cardButton setBackgroundImage:[self backGroundImageForCard:card]
                              forState:UIControlStateNormal];
        cardButton.enabled = !card.isMatched;
        
        self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
    }
}

- (NSString *)titleForCard:(Card *)card
{
    return card.isChosen ? card.contents : @"";
}

- (UIImage *)backGroundImageForCard:(Card *)card
{
    return [UIImage imageNamed:card.isChosen ? @"cardfront" : @"vff_cardback"];
}

- (IBAction)deal
{
    self.game = nil;
    self.numberOfCardsSegmentedControl.enabled = YES;
    self.lastFlipLabel.text = @"";
    [self updateUI];
}

- (IBAction)numberOfCardsSwitch:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == 0) {
        self.numberOfCards = 2;
    } else if (sender.selectedSegmentIndex == 1) {
        self.numberOfCards = 3;
    }
    
    self.game.numberOfCards = self.numberOfCards;
}
- (IBAction)historySlider:(UISlider *)sender
{
    if (([self.game.flipHistory count]) && (sender.value < sender.maximumValue))
    {
        int index = (int)(self.historySlider.value * [self.game.flipHistory count]);
        self.lastFlipLabel.textColor = [UIColor grayColor];
        self.lastFlipLabel.text = [self.game.flipHistory objectAtIndex:index];
        [self updateUI];
    }
}

@end

