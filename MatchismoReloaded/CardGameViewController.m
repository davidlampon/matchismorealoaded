//
//  CardGameViewController.m
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 06/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import "CardGameViewController.h"
#import "Grid.h"

@interface CardGameViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) Deck *deck;
@property (nonatomic, strong) CardMatchingGame *game;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *numberOfCardsSegmentedControl;
// Play history
@property (nonatomic) int numberOfCards;

//Settings
@property (strong, nonatomic) GameSettings *gameSettings;

// Collection View
@property (weak, nonatomic) IBOutlet UICollectionView *cardCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *threeCardsMoreButton;
@property (strong, nonatomic) Grid *grid;
@end

@implementation CardGameViewController

- (BOOL)redeal
{
    if (!_redeal) _redeal = FALSE;
    return _redeal;
}

- (Grid *)grid
{
    if (!_grid) _grid = [[Grid alloc] init];
    
    _grid.cellAspectRatio = 0.5;
    _grid.size = self.cardCollectionView.bounds.size;
    _grid.minimumNumberOfCells = self.game.cardCount;
    
    return _grid;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.grid.cellSize;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.game.cardCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Card" forIndexPath:indexPath];
    Card *card = [self.game cardAtIndex:indexPath.item];
    [self updateCell:cell usingCard:card animate:NO redeal:self.redeal];
    //NSLog(@"%@",(self.redeal ? @"TRUE" : @"FALSE"));
    return cell;
}

- (void)updateCell:(UICollectionViewCell *)cell usingCard:(Card *)card animate:(BOOL)animate redeal:(BOOL)redeal
{
    // abstract
}

- (GameSettings *)gameSettings
{
    if (!_gameSettings) _gameSettings = [[GameSettings alloc] init];
    return _gameSettings;
}

- (GameResult *)gameResult
{
    if (!_gameResult) _gameResult = [[GameResult alloc] init];
    _gameResult.gameType = self.gameType;
    return _gameResult;
}

- (CardMatchingGame *)game
{
    if (!_game) _game = [[CardMatchingGame alloc] initWithCardCount:self.startingCardCount
                                                          usignDeck:self.deck];
    _game.numberOfCards = self.numberOfCards;
    _game.matchBonus = self.gameSettings.matchBonus;
    _game.mismatchPenalty = self.gameSettings.mismatchPenalty;
    _game.flipCost = self.gameSettings.flipCost;
    
    return _game;
}

- (Deck *)deck
{
    if (!_deck) _deck = [self createDeck];
    return _deck;
}

- (Deck *)createDeck
{
    return nil;
}

- (IBAction)touchCardButton:(UIGestureRecognizer *)gesture
{
    CGPoint tapLocation = [gesture locationInView:self.cardCollectionView];
    NSIndexPath *indexPath = [self.cardCollectionView indexPathForItemAtPoint:tapLocation];
    if (indexPath)
    {
        [self.game chooseCardAtIndex:indexPath.item];       
        [self updateUI];
    }
}

- (void)updateUI
{
    for (UICollectionViewCell *cell in [self.cardCollectionView visibleCells])
    {
        NSIndexPath *indexPath = [self.cardCollectionView indexPathForCell:cell];
        Card *card = [self.game cardAtIndex:indexPath.item];

        if (card.isMatched)
        {
            //remove from the array and reload data
            [self.game removeCard:card];
            [self.cardCollectionView deleteItemsAtIndexPaths:[[NSArray alloc] initWithObjects:indexPath, nil]];
        } else {
            [self updateCell:cell usingCard:card animate:YES redeal:self.redeal];
        }
    }
    
    if (self.redeal == TRUE)
    {
        self.redeal = FALSE;        
    }
    
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
    self.gameResult.score = self.game.score;
}

- (UIImage *)backGroundImageForCard:(Card *)card
{
    return [UIImage imageNamed:card.isChosen ? @"cardfront" : @"vff_cardback"];
}

- (IBAction)deal
{
    self.game = nil;
    self.numberOfCardsSegmentedControl.enabled = YES;
    self.gameResult = nil;
    self.redeal = TRUE;
    self.threeCardsMoreButton.alpha = 1.0;
    self.deck = nil;
    [self.cardCollectionView reloadData];
}

- (IBAction)numberOfCardsSwitch:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == 0) {
        self.numberOfCards = 2;
    } else if (sender.selectedSegmentIndex == 1) {
        self.numberOfCards = 3;
    }
    
    self.game.numberOfCards = self.numberOfCards;
}
- (IBAction)threeCardsMore
{
    self.redeal = TRUE;
    [self.game addThreeCardsToGameWithDeck:self.deck];
    
    //[self.cardCollectionView reloadData];
    NSMutableArray *indexPathsAtTheEnd = [[NSMutableArray alloc] init];
    
    for (int i = self.game.cardCount-3; i < self.game.cardCount; i++)
    {
        [indexPathsAtTheEnd addObject:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    
    [self.cardCollectionView insertItemsAtIndexPaths:indexPathsAtTheEnd];
    
    if (![self.deck count])
    {
        self.threeCardsMoreButton.alpha = 0.0;
    }
    
    [self updateUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.game.matchBonus = self.gameSettings.matchBonus;
    self.game.mismatchPenalty = self.gameSettings.mismatchPenalty;
    self.game.flipCost = self.gameSettings.flipCost;
}

@end
