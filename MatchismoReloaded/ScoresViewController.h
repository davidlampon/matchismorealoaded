//
//  ScoresViewController.h
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 18/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GameResult.h"

@interface ScoresViewController : UIViewController

@end