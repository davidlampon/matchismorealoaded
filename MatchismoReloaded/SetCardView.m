//
//  SetCardView.m
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 26/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import "SetCardView.h"

@interface SetCardView()
@property (nonatomic) CGFloat faceCardScaleFactor;
@end

@implementation SetCardView

#pragma mark - Properties

#define DEFAULT_FACE_CARD_SCALE_FACTOR 0.90

@synthesize faceCardScaleFactor = _faceCardScaleFactor;

- (CGFloat)faceCardScaleFactor
{
    if (!_faceCardScaleFactor) _faceCardScaleFactor = DEFAULT_FACE_CARD_SCALE_FACTOR;
    return _faceCardScaleFactor;
}

- (void)setFaceCardScaleFactor:(CGFloat)faceCardScaleFactor
{
    _faceCardScaleFactor = faceCardScaleFactor;
    [self setNeedsDisplay];
}

- (void)setNumber:(NSUInteger)number
{
    _number = number;
    [self setNeedsDisplay];
}

- (void)setShading:(NSUInteger)shading
{
    _shading = shading;
    [self setNeedsDisplay];
}

- (void)setColor:(NSUInteger)color
{
    _color = color;
    [self setNeedsDisplay];
}

- (void)setFaceUp:(BOOL)faceUp
{
    _faceUp = faceUp;
    [self setNeedsDisplay];
}

#pragma mark - Gesture Handling

- (void)pinch:(UIPinchGestureRecognizer *)gesture
{
    if ((gesture.state == UIGestureRecognizerStateChanged) ||
        (gesture.state == UIGestureRecognizerStateEnded)) {
        self.faceCardScaleFactor *= gesture.scale;
        gesture.scale = 1.0;
    }
}

#pragma mark - Drawing

#define CORNER_FONT_STANDARD_HEIGHT 180.0
#define CORNER_RADIUS 12.0

- (CGFloat)cornerScaleFactor { return self.bounds.size.height / CORNER_FONT_STANDARD_HEIGHT; }
- (CGFloat)cornerRadius { return CORNER_RADIUS * [self cornerScaleFactor]; }
- (CGFloat)cornerOffset { return [self cornerRadius] / 3.0; }

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:[self cornerRadius]];
    
    [roundedRect addClip];
    
    [[UIColor whiteColor] setFill];
    UIRectFill(self.bounds);
    
    [[UIColor blackColor] setStroke];
    [roundedRect stroke];
    
    if (self.faceUp) {
        UIImage *faceImage = [UIImage imageNamed:[NSString stringWithFormat:@"%d%@%d%d", self.number, self.symbol, self.shading, self.color]];
        if (faceImage) {
            CGRect imageRect = CGRectInset(self.bounds,
                                           self.bounds.size.width * (1.0-self.faceCardScaleFactor),
                                           self.bounds.size.height * (1.0-self.faceCardScaleFactor));
            [faceImage drawInRect:imageRect];
        } else {
            [self drawSymbols];
        }
        
    } else {
        [[UIImage imageNamed:@"vff_cardback"] drawInRect:self.bounds];
    }
}

#pragma mark - Symbols

- (void)drawSymbols
{
    UIBezierPath *path = [[UIBezierPath alloc] init];
    
    // Symbol - triangle
    if ([self.symbol isEqualToString:@"▲"]){
        [path moveToPoint: CGPointMake(75, 0)];
        [path addLineToPoint:CGPointMake(150, 150)];
        [path addLineToPoint:CGPointMake(0, 150)];
        [path closePath];
    } else if ([self.symbol isEqualToString:@"●"]) {
        path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, 150, 150)];
    } else if ([self.symbol isEqualToString:@"◼︎"]) {
        [path moveToPoint: CGPointMake(0, 0)];
        [path addLineToPoint:CGPointMake(150, 0)];
        [path addLineToPoint:CGPointMake(150, 150)];
        [path addLineToPoint:CGPointMake(0, 150)];
        [path closePath];
    }
    
    // Color
    UIColor *symbolColor = [[UIColor alloc] init];
    
    switch (self.color)
    {
        case 1:
            symbolColor = [UIColor redColor];
            break;
        case 2:
            symbolColor = [UIColor greenColor];
            break;
        case 3:
            symbolColor = [UIColor purpleColor];
            break;
    }
    
    [symbolColor setStroke];
    
    // Fill
    switch (self.shading)
    {
        case 1:
            symbolColor = [symbolColor colorWithAlphaComponent:1.0];
            break;
        case 2:
            symbolColor = [symbolColor colorWithAlphaComponent:0.5];
            break;
        case 3:
            symbolColor = [symbolColor colorWithAlphaComponent:0.0];
            break;
    }
    
    [symbolColor setFill];
    
    // Scale to fit
    CGFloat axisScale = (((self.bounds.size.width/2)/3)/path.bounds.size.height);
  
    CGAffineTransform scale = CGAffineTransformMakeScale(axisScale, axisScale);
    [path applyTransform:scale];
    
    CGAffineTransform moveTo;
    
    if ((self.number == 1) || (self.number == 3)) {
        moveTo = CGAffineTransformMakeTranslation((self.bounds.size.width/2-path.bounds.size.width/2), (self.bounds.size.height/2-path.bounds.size.height/2));
        [path applyTransform:moveTo];
        [path fill];
        [path stroke];
    } else if (self.number == 2) {
        moveTo = CGAffineTransformMakeTranslation((self.bounds.size.width/2-path.bounds.size.width*3/2), (self.bounds.size.height/2-path.bounds.size.height/2));
        [path applyTransform:moveTo];
        [path fill];
        [path stroke];
        
        moveTo = CGAffineTransformMakeTranslation(path.bounds.size.width*2, 0);
        [path applyTransform:moveTo];
        [path fill];
        [path stroke];
    }
    
    if (self.number == 3){
        moveTo = CGAffineTransformMakeTranslation(-path.bounds.size.width*3/2, 0);
        [path applyTransform:moveTo];
        [path fill];
        [path stroke];
        
        moveTo = CGAffineTransformMakeTranslation(path.bounds.size.width*3, 0);
        [path applyTransform:moveTo];
        [path fill];
        [path stroke];
    }
}

#pragma mark - Initialization

- (void)setup
{
    self.backgroundColor = nil;
    self.opaque = NO;
    self.contentMode = UIViewContentModeRedraw;
}

- (void)awakeFromNib
{
    [self setup];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self setup];
    return self;
}

@end

