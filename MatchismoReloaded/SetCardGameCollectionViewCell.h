//
//  SetCardGameCollectionViewCell.h
//  MatchismoReloaded
//
//  Created by David Lampon Diestre on 26/11/13.
//  Copyright (c) 2013 Three Bytes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetCardView.h"

@interface SetCardGameCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet SetCardView *setCardView;
@end
